# all-the-things
Test playbook for Tower

Configure NTP client
Configure /etc/resolv.conf DNS with svcs
Add autofs maps to /etc/auto.master
Install nagios client
Configure custom sudoers file
Configure custom nsswitch.conf file
Set/Change root passwords
Configure custom syslog config
Create ZFS filesystems
Add cronjobs
Install opencsw package - example: lsof - https://www.opencsw.org/packages/lsof/
Install openjdk

